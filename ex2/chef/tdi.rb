user 'tdi' do
  supports :manage_home => true
  action :create
  manage_home true
  home '/home/tdi'
end

template '/home/tdi/.tdi' do
  action :create
  source 'bogus'
end
