require 'colorize'

class ColorSay < Thor
  desc "hello", "Say hello in color"
  def hello
    puts "Hello".colorize( :red )
    puts "Hello".colorize( :green )
    puts "Hello".colorize( :yellow )
  end
end
