#
# Cookbook Name:: irc
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

user 'tdi' do
  supports :manage_home => true
  action :create
  manage_home true
  home '/home/tdi'
end

package 'irssi' do
  action :install
end

directory '/home/tdi/.irssi' do
  action :create
  owner 'tdi'
  group 'tdi'
end

cookbook_file '/home/tdi/.irssi/config' do
  action :create
  owner 'tdi'
  group 'tdi'
  mode '0755'
  source 'irssi-config'
end

